<?php 
  
    include("heard.php");
    include("connection.php");
    include("crud-relatorio.php");

    echo '<center><h1><b>Relatório 1 - Listar Justificativa(s) de Ponto <b></h1></center>
         <hr>';

    echo '<table class="table table-striped table-bordered">
    <tr>
      <th> Nº </th>
      <th> Código </th> 
      <th> Nome </th>
      <th> Curso </th>
      <th> Turno </th>
      <th> Data </th>
      <th> Entrada </th>
      <th> Saída </th>
      <th> Tipo </th>
      <th> Justificativa </th>
      <th> Status </th>
      <th> Data </th> </tr>';

    $contador = 1;

     $justificativas = selectJustificativaPontoStatus($conexao);
     foreach ($justificativas as $justificativa):
 
        echo '<tr>';
        echo '<td>'.$contador.'</td>';
        echo '<td>'.$justificativa['IDJP'].'</td>';
        echo '<td>'.$justificativa['NOME'].'</td>';
        echo '<td>'.$justificativa['CURSO'].'</td>';
        echo '<td>'.$justificativa['TURNO'].'</td>';
        echo '<td>'.$justificativa['DATA'].'</td>';         
        echo '<td>'.$justificativa['ENTRADA'].'</td>';         
        echo '<td>'.$justificativa['SAIDA'].'</td>';  
        echo '<td>'.$justificativa['TIPO'].'</td>';  
        echo '<td>'.$justificativa['JUSTIFICATIVA'].'</td>';  
        echo '<td>'.$justificativa['STATUS'].'</td>';  
        echo '<td>'.$justificativa['DTHRCAD'].'</td>';  
        echo '</tr>';      

        $contador++;
  
        endforeach;

        echo '</table>';
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('d-m-Y H:i');

    echo '<table class="table">
   
    <tr>
      <th style="text-align: right;">Data/Hora Emissão: '.$date.'</th>
    </tr>';
    echo '</table>';






?>

<?php include("footer.php");?>
