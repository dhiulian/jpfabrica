<?php      

	  include("heard.php");
    include("connection.php");
    include("crud-relatorio.php");?>

  	<h1><b>Lista de Relatório(s)</h1></b><br>
  
      <table class="table" style="text-align:left">
      
      <tr>
        <th> Código </th>
        <th> Título </th>
        <th> Operação</th>
        
      </tr>

        <tr> 
          <td>
            1
          </td> 
          <td>
            Listar Justificativa(s) de Ponto
          </td> 
          <td><a class = "btn btn-primary" href="form-select-relatorio-status.php">Vizualizar</a></td>
        </tr>

        <tr> 
          <td>
            1.01
          </td> 
          <td>
             Listar JP c/status - Aberta 
          </td> 
          <td><a class = "btn btn-primary" href="form-select-relatorio-status-1.php">Vizualizar</a></td>
        </tr>


        <tr> 
          <td>1.02         
          </td> 
          <td>
             Listar JP c/status - Deferida 
          </td> 
          <td><a class = "btn btn-primary" href="form-select-relatorio-status-2.php">Vizualizar</a></td>
        </tr>

        <tr> 
          <td>1.03          
          </td> 
          <td>
            Listar JP c/status - Indeferida 
          </td> 
          <td><a class = "btn btn-primary" href="form-select-relatorio-status-3.php">Vizualizar</a></td>
        </tr>

        <tr> 
          <td>1.04          
          </td> 
          <td>
            Listar Justificativa(s) de Ponto p/período 
          </td> 
          <td><a class = "btn btn-primary" href="form-relatorio-periodo.php">Vizualizar</a></td>
        </tr>

      </table>


<?php include("footer.php");?>