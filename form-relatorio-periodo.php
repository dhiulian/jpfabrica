<?php     
    
    include("heard.php");
    include("connection.php");
    include("crud-relatorio.php");
?>

  <h1><b>Relatório 1.04 - Listar Justificativa(s) de Ponto p/período</h1><b><br><br>
    
    <form action="select-relatorio-periodo.php" method="post">

      <table class="table">

        <tr> 
          <td><b>Informe a Data Inicial: </td>  
        	<td> <input class="form-control" type="date" 
                name="data1" required autofocus/><br/></td>
        </tr>

        <tr> 
          <td><b>Informe a Data Final: </td>  
          <td> <input class="form-control" type="date" 
                name="data2" required autofocus/><br/></td>
        </tr>


		<tr>
		  <td><input class="btn btn-primary" type="submit" value="Imprimir"/>
              <input class="btn btn-primary" type="reset"  value="Limpar"/>
          </td>
          <td> </td>  
		</tr>

      </table>
    </form>

<?php include("footer.php");?>