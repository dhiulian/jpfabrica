<?php

	function selectJustificativaPontoStatus1($conexao){

		$justificativas = array();
		$query = "SELECT IDJP,CONCAT(CPF,' - ',NOME) AS NOME,CONCAT(IDCU,' - ',CURSO) AS CURSO,CONCAT(IDTU,' - ',TURNO) AS      TURNO,DATA, ENTRADA, SAIDA, CONCAT(IDTJ,' - ',TIPO) AS TIPO, JUSTIFICATIVA, 
				  CONCAT(IDSJ,' - ',STATUS) AS STATUS, DTHRCAD 
				  FROM V_P_JUSTIFICATIVAPONTO 
				  WHERE IDSJ = 1 
				  ORDER BY DTHRCAD DESC";

		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}

	function selectJustificativaPontoStatus2($conexao){

		$justificativas = array();
		$query = "SELECT IDJP,CONCAT(CPF,' - ',NOME) AS NOME,CONCAT(IDCU,' - ',CURSO) AS CURSO,CONCAT(IDTU,' - ',TURNO) AS      TURNO,DATA, ENTRADA, SAIDA, CONCAT(IDTJ,' - ',TIPO) AS TIPO, JUSTIFICATIVA, 
				  CONCAT(IDSJ,' - ',STATUS) AS STATUS, DTHRCAD 
				  FROM V_P_JUSTIFICATIVAPONTO 
				  WHERE IDSJ = 2 
				  ORDER BY DTHRCAD DESC";

		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}

	function selectJustificativaPontoStatus3($conexao){

		$justificativas = array();
		$query = "SELECT IDJP,CONCAT(CPF,' - ',NOME) AS NOME,CONCAT(IDCU,' - ',CURSO) AS CURSO,CONCAT(IDTU,' - ',TURNO) AS      TURNO,DATA, ENTRADA, SAIDA, CONCAT(IDTJ,' - ',TIPO) AS TIPO, JUSTIFICATIVA, 
				  CONCAT(IDSJ,' - ',STATUS) AS STATUS, DTHRCAD 
				  FROM V_P_JUSTIFICATIVAPONTO 
				  WHERE IDSJ = 3 
				  ORDER BY DTHRCAD";

		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}


	function selectJustificativaPontoStatus($conexao){

		$justificativas = array();
		$query = "SELECT IDJP,CONCAT(CPF,' - ',NOME) AS NOME,CONCAT(IDCU,' - ',CURSO) AS CURSO,CONCAT(IDTU,' - ',TURNO) AS      TURNO,DATA, ENTRADA, SAIDA, CONCAT(IDTJ,' - ',TIPO) AS TIPO, JUSTIFICATIVA, 
				  CONCAT(IDSJ,' - ',STATUS) AS STATUS, DTHRCAD 
				  FROM V_P_JUSTIFICATIVAPONTO 
				  ORDER BY STATUS, DTHRCAD, NOME";

		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}


	function selectJustificativaPontoStatusPeriodo($conexao,$data1,$data2){

		$justificativas = array();
		$query = "SELECT IDJP,CONCAT(CPF,' - ',NOME) AS NOME,CONCAT(IDCU,' - ',CURSO) AS CURSO,CONCAT(IDTU,' - ',TURNO) AS      TURNO,DATA, ENTRADA, SAIDA, CONCAT(IDTJ,' - ',TIPO) AS TIPO, JUSTIFICATIVA, 
				  CONCAT(IDSJ,' - ',STATUS) AS STATUS, DTHRCAD 
				  FROM V_P_JUSTIFICATIVAPONTO
				  ORDER BY DTHRCAD, STATUS, NOME";

		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}


?>