 <?php

     include("heard-justificativaPonto.php");
     include("connection.php");
     include("crud-justificativaPonto.php");

     date_default_timezone_set('America/Sao_Paulo');
     $datehora = date('d-m-Y H:i');

	$curso = $_POST["curso"];
	$turno = $_POST["turno"];
	$data = $_POST["data"];
	$horaE  = $_POST["horaE"];
	$horaS = $_POST["horaS"];
	$tipo = $_POST["tipo"];
	$justificativa = $_POST["justificativa"];

	if(insertJustificativaPonto($conexao,$curso,$turno,$data,$horaE,$horaS,
          $tipo,$justificativa)) 
     {?>

		<h2>
               <p class="text-success">Registro de Justificativa de Ponto gravado c/sucesso!
               </p>
          </h2>
          <br>
          <table class="table" style="text-align:left">
          <tr> 
          <td><b>Nº do CPF: </b><?= $curso;?></td>
          </tr> 
          <tr> 
          <td><b>Código do Curso: </b><?= $curso;?></td>
          </tr> 
          <tr> 
          <td><b>Turno: </b><?= $turno;?></td>
          </tr> 
          <tr> 
          <td><b>Data: </b><?= $data;?></td>
          </tr>
          <tr> 
          <td><b>Hora Entrada: </b><?= $horaE;?></td>
          </tr> 
          <tr> 
          <td><b>Hora Saida: </b><?= $horaS;?></td>
          </tr>
          <tr> 
          <td><b>Tipo: </b><?= $tipo;?></td>
          </tr>
          <tr> 
          <td><b>Justificativa: </b><?= $justificativa;?></td>
          </tr>
          <tr> 
          <td><b>Status: </b>JP aberta em <?= $datehora;?></td>
          </tr>
          </table>
	<?php } else { 

		$msg = mysqli_error($conexao);
          echo $msg;
	?>

	<h2><p class="text-danger">Atenção: Registro de Justificativa de Ponto não gravado. </p></h2><br>



    <?php
		}
    ?>

<?php include("footer.php");?>
