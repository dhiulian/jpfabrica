<?php 
  
  include("heard-justificativaPonto.php");
  include("connection.php");
  include("crud-justificativaPonto.php");

  $id = $_GET['idx'];
  $justificativa = selectUpdateJustificativaPonto($conexao,$id);

?>

  <h1><b>Formulário de Edição de Justificativa de Ponto</h1></b><br>
    
    <form action="update-justificativaPonto.php" method="post">

      <table class="table">

        <tr> 
          <td><b>Código do Curso:</b></td>  
        	<td> 
            <input class="form-control" type="text" name="curso" value="<?=$justificativa['CURSO']?>" autofocus/>
          </td>
        </tr>

        <tr> 
          <td><b>Código do Turno:</b></td>  
          <td> 
            <input class="form-control" type="text" name="turno" value="<?=$justificativa['TURNO']?>" />
          </td>
        </tr>

        <tr> 
          <td><b>Data:</b></td>  
          <td> <input class="form-control" type="date"  value="<?=$justificativa['DATA']?>" name="data"/><br/></td>
        </tr>

        <tr>  
          <td><b>Hora Entrada:</b></td>  
          <td> <input class="form-control" type="time" name="horaE" 
                      value="<?=$justificativa['ENTRADA']?>" /><br/>
          </td>
        </tr>


        <tr>  
          <td><b>Hora Saída:</b></td>  
          <td> <input class="form-control" type="time" name="horaS" value="<?=$justificativa['SAIDA']?>"/><br/>
          </td>
        </tr>

        <tr> 
          <td><b>Código Tipo Justificativa:</b></td>  
          <td> 
            <input class="form-control" type="text" name="tipo" value="<?=$justificativa['TIPO']?>" autofocus/>
          </td>
        </tr>

        <tr>  
          <td><b>Justificativa: </td>  
          <td> <input class="form-control" type="text" name="justificativa" value="<?=$justificativa['JUSTIFICATIVA']?>"/><br/></td>
        </tr>


  <input type="hidden" name="idx" value="<?=$justificativa['IDJP']?>"/>


		<tr>
		  <td><input class="btn btn-success" type="submit" value="Gravar"/>
              <input class="btn btn-light" type="reset"  value="Cancelar" />
          </td>
          <td> </td>  
		</tr>

      </table>
    </form>

<?php include("footer.php");?>