<?php include("heard-justificativaPonto.php");
      include("connection.php");
      include("crud-justificativaPonto.php");
?>

  <h1><b>Lista de Justificativa(s)</h1></b><br>

	<?php
		if(array_key_exists("removido", $_GET) && $_GET["removido"]=="true") {
	?>
		<h2> 
			<p class="text-danger"> Registro de Justificativa de Ponto excluido c/sucesso.</p>
		</h2><br>
	<?php
		}
	?>

<table class="table table-striped table-bordered">

		<tr><b>
			<th> Código </th>
			<th> Nome </th>
			<th> Curso </th>
			<th> Turno </th>
			<th> Data </th>
			<th> Entrada </th>
			<th> Saída </th>
			<th> Tipo </th>
			<th> Justificativa</th>
			<th> Status </th>
			<th> Data/Hora </th>
			<th> Operação </th>
			<th> Operação </th>
		</tr></b>


	<?php
		$justificativas = selectJustificativaPonto($conexao);
		foreach ($justificativas as $justificativa) :
	?>
		<tr>
			<td><?= $justificativa['IDJP'] ?></td>
			<td><?= $justificativa['NOME'] ?></td>
			<td><?= $justificativa['CURSO'] ?></td>
			<td><?= $justificativa['TURNO'] ?></td>
			<td><?= $justificativa['DATA'] ?></td>
			<td><?= $justificativa['ENTRADA'] ?></td>
			<td><?= $justificativa['SAIDA'] ?></td>
			<td><?= $justificativa['TIPO'] ?></td>
			<td><?= $justificativa['JUSTIFICATIVA'] ?></td>
			<td><?= $justificativa['STATUS'] ?></td>
			<td><?= $justificativa['DTHRCAD'] ?></td>
			<td><a class = "btn btn-primary" href="form-update-justificativaPonto.php?idx=<?=$justificativa['IDJP']?>">Editar</a></td>
			

			<td>
				<form action="delete-justificativaPonto.php" method = "post">
					<input type="hidden" name="idx" value="<?=$justificativa['IDJP']?>">
					<button class="btn btn-danger">Excluir</button> 
				</form>
			</td>




		</tr>
	<?php
		endforeach
	?>

</table>

<?php include("footer.php");?>