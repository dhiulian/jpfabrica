<?php

	function selectJustificativaPonto($conexao){

		$justificativas = array();
		$query = "SELECT * FROM V_P_JUSTIFICATIVAPONTO";
		$resultado = mysqli_query($conexao, $query);
		
		while ($justificativa = mysqli_fetch_assoc($resultado)) 
			  {
					array_push($justificativas, $justificativa);
			  }
		return $justificativas;
	}

	function deleteJustificativaPonto($conexao,$id){

		$query = "DELETE FROM PJUSTIFICATIVAPONTO WHERE IDJP = '{$id}'";
		mysqli_query($conexao,$query);
		return mysqli_query($conexao,$query);
	}

	function insertJustificativaPonto($conexao,$curso,$turno,
		$data,$horaE,$horaS,$tipo,$justificativa){

		$query = "INSERT INTO PJUSTIFICATIVAPONTO (COLABORADOR,CURSO,TURNO,DATA,
		ENTRADA,SAIDA,TIPO,JUSTIFICATIVA,STATUS,DTHRCAD)	
		               VALUES ('44444444444','{$curso}','{$turno}', 
		               '{$data}','{$horaE}','{$horaS}','{$tipo}','{$justificativa}','1',NOW())";

	    return mysqli_query($conexao,$query);
    }

	function updateJustificativaPonto($conexao,$id,$curso,$turno,
		$data,$horaE,$horaS,$tipo,$justificativa){

		$query = "UPDATE PJUSTIFICATIVAPONTO 
		             SET CURSO = '{$curso}', 
		                 TURNO = '{$turno}', 
		                 DATA = '{$data}',  
		                 ENTRADA = '{$horaE}', 
		                 SAIDA   = '{$horaS}',
		                 TIPO = '{$tipo}',
		                 JUSTIFICATIVA = '{$justificativa}',
		                 DTHRCAD = NOW()
		           WHERE IDJP = '{$id}'";

		return mysqli_query($conexao, $query);
	}


	function selectUpdateJustificativaPonto($conexao,$id){
	
		$query = "SELECT * FROM PJUSTIFICATIVAPONTO WHERE IDJP = {$id}";
		
		$resultado = mysqli_query($conexao, $query);
		
		return mysqli_fetch_assoc($resultado);
	}







?>