<?php 
	
  include("heard-justificativaPonto.php");
  include("connection.php");
  include("crud-justificativaPonto.php");
  
  $id = $_POST['idx'];

  $curso = $_POST['curso'];
  $turno = $_POST['turno'];
  $data = $_POST['data'];
  $horaE  = $_POST['horaE'];
  $horaS = $_POST['horaS'];
  $tipo = $_POST['tipo'];
  $justificativa = $_POST['justificativa'];

  if(updateJustificativaPonto($conexao,$id,$curso,$turno,
		$data,$horaE,$horaS,$tipo,$justificativa)) 
	{?>
		<h1><p class="text-success">Registro de Justificativa de Ponto editado c/sucesso!</p></h1><br><br>

          <table class="table" style="text-align:left">
          <tr> 
          <td><b>Código do Curso: </b><?= $curso;?></td>
          </tr> 
          <tr> 
          <td><b>Código do Turno: </b><?= $turno;?></td>
          </tr> 
          <tr> 
          <td><b>Data: </b><?= $data;?></td>
          </tr>
          <tr> 
          <td><b>Hora Entrada: </b><?= $horaE;?></td>
          </tr> 
          <tr> 
          <td><b>Hora Saida: </b><?= $horaS;?></td>
          </tr>
          <tr> 
          <td><b>Código Tipo: </b><?= $tipo;?></td>
          </tr>
          <tr> 
          <td><b>Justificativa: </b><?= $justificativa;?></td>
          </tr>
          </table>

	<?php } else { 

		$msg = mysqli_error($conexao);
	?>

    
    <h1><p class="text-danger">Atenção: Registro de Justificativa de Ponto não editado.</p></h1><br><br> 

		<table class="table">
			<tr> 
          		<td><b>Número do CPF:  </b><?= $cpf;?></td>
          	</tr>			
			<tr> 
          		<td><b>Nome Completo:  </b><?= $nome;?></td>
          	</tr>			
			<tr> 
          		<td><b>Sexo:  </b><?= 'X-X-X';?></td>
          	</tr>			
			<tr> 
          		<td><b>Data de Nascimento:  </b><?= 'X-X-X';?></td>
          	</tr>			
			<tr> 
          		<td><b>Endereço:  </b><?= 'X-X-X';?></td>
          	</tr>
			<tr> 
          		<td><b>Telefone Móvel:  </b><?= 'X-X-X';?></td>
          	</tr>
			<tr> 
          		<td><b>Telefone Fixo:  </b><?= 'X-X-X';?></td>
          	</tr>
			<tr> 
          		<td><b>Telefone Comercial:  </b><?= 'X-X-X';?></td>
          	</tr>
			<tr> 
          		<td><b>E-mail:  </b><?= 'X-X-X';?></td>
          	</tr>

			</tr>
			<tr> 
          		<td><b>Data de Cadastro:  </b><?= 'X-X-X';?></td>
          	</tr>


		</table>



<?php
		}
?>

<?php include("footer.php");?>
