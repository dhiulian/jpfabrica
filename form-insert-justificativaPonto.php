<?php include("heard-justificativaPonto.php");?>

  <h1><b>Formulário de Justificativa de Ponto</h1></b><br>
    
    <form action="insert-justificativaPonto.php" method="post">

      <table class="table">

        <tr> 
          <td><b>Nome do Curso:</b></td>  
        	<td> 
            <select class="form-control" id="curso"  name="curso">
              <option value="" disabled selected></option>
              <option value="1">1 - Administração</option>
              <option value="2">2 - Ciência da Computação</option>
              <option value="3">3 - Engenharia Ambiental</option>
              <option value="4">4 - Engenharia Civil</option>
            </select> 
          </td>
        </tr>

        <tr> 
          <td><b>Turno:</b></td>  
          <td> 
            <select class="form-control" id="turno" name="turno">
              <option value="" disabled selected> 
              </option>
              <option value="1">1 - Manhã</option>
              <option value="2">2 - Tarde</option>
              <option value="3">3 - Noite</option>
            </select> 
          </td>
        </tr>

        <tr> 
          <td><b>Data:</b></td>  
          <td> <input class="form-control" type="date"  value=" - Selecione a Data"
                name="data"/><br/></td>
        </tr>

        <tr>  
          <td><b>Hora Entrada:</b></td>  
          <td> <input class="form-control" type="time" name="horaE" required /><br/>
          </td>
        </tr>


        <tr>  
          <td><b>Hora Saída:</b></td>  
          <td> <input class="form-control" type="time" name="horaS" required/><br/>
          </td>
        </tr>

        <tr> 
          <td><b>Tipo Justificativa:</b></td>  
          <td> 
            <select class="form-control" id="tipo" name="tipo">
              <option value="" disabled selected></option>
              <option value="1">1 - Abono </option>
              <option value="2">2 - Reposição </option>

            </select> 
          </td>
        </tr>

        <tr>  
          <td><b>Justificativa: </td>  
          <td> <input class="form-control" type="text" name="justificativa" required/><br/></td>
        </tr>


		<tr>
		  <td><input class="btn btn-success" type="submit" value="Gravar"/>
              <input class="btn btn-light" type="reset"  value="Cancelar" />
          </td>
          <td> </td>  
		</tr>

      </table>
    </form>

<?php include("footer.php");?>